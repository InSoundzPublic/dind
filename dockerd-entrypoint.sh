#!/bin/sh

set -eux

if [ "$#" -eq 0 ] || [ "${1#-}" != "$1" ]; then
    sudo /usr/sbin/sshd
	set -- dockerd \
		--tls=false \
		--host=$DOCKER_HOST \
		--add-runtime=nvidia=/usr/bin/nvidia-container-runtime \
		"$@"

    sudo mkdir -p /unmasked-proc
    sudo mount -t proc proc /unmasked-proc
    sudo mkdir -p /unmasked-sys
    sudo mount -t sysfs sysfs /unmasked-sys
    sudo mkdir -p /dev/net
    sudo mknod /dev/net/tun c 10 200 || :
    sudo chmod 666 /dev/net/tun
    exec sudo setpriv --reuid=rootless --regid=rootless \
                              --init-groups --reset-env "$0" "$@"
    echo 50000 | sudo tee /unmasked-proc/sys/kernel/keys/maxkeys
    sudo find /run /var/run -iname 'docker*.pid' -delete || :
fi 

if [ "$1" = 'dockerd' ]; then

	uid="$(id -u)"
	: "${XDG_RUNTIME_DIR:=/run/user/$uid}"
	PATH=/usr/local/sbin:/usr/sbin:/sbin:${PATH}
	export XDG_RUNTIME_DIR PATH

	exec dumb-init rootlesskit \
		--pidns \
		--disable-host-loopback \
		--net=slirp4netns \
		--port-driver=builtin \
		--copy-up=/etc \
		--copy-up=/run \
		"$@"
fi

exec "$@"
