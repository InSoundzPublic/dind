FROM nvidia/cuda:11.2.0-cudnn8-devel-ubuntu20.04

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get -y install curl gnupg

RUN install -m 0755 -d /etc/apt/keyrings
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg
RUN chmod a+r /etc/apt/keyrings/docker.gpg

RUN echo \
	    "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
	    "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
	    tee /etc/apt/sources.list.d/docker.list > /dev/null

RUN distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
             && curl -fsSL https://nvidia.github.io/libnvidia-container/gpgkey | gpg --dearmor -o /usr/share/keyrings/nvidia-container-toolkit-keyring.gpg \
             && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | \
             sed 's#deb https://#deb [signed-by=/usr/share/keyrings/nvidia-container-toolkit-keyring.gpg] https://#g' | \
             tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

RUN apt-get update
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y dumb-init slirp4netns iptables iproute2
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y nvidia-container-toolkit nvidia-container-runtime
RUN DEBIAN_FRONTEND=noninteractive apt-get install -y uidmap sudo

RUN sed -i "s;\#no-cgroups = false;no-cgroups = true;g" /etc/nvidia-container-runtime/config.toml
ENV DOCKER_HOST=unix:///run/user/1000/docker.sock

RUN DEBIAN_FRONTEND=noninteractive apt-get install -y openssh-server vim

RUN mkdir /var/run/sshd
RUN mkdir /root/.ssh
RUN echo 'root:root' |chpasswd
RUN sed -ri 's/^#?PermitUserEnvironment\s+.*/PermitUserEnvironment yes/' /etc/ssh/sshd_config
RUN sed -ri 's/^#?PermitRootLogin\s+.*/PermitRootLogin yes/' /etc/ssh/sshd_config
RUN sed -i "s;\#PasswordAuthentication yes;PasswordAuthentication yes;g" /etc/ssh/sshd_config
COPY environment /root/.ssh/
RUN echo "DOCKER_HOST=unix:///run/user/1000/docker.sock" >> /root/.ssh/environment

ENV NOTVISIBLE "in users profile"
RUN echo "export VISIBLE=now" >> /etc/profile

RUN mkdir -p /run/user \
    && chmod 1777 /run/user \
    && adduser --home /home/rootless --disabled-password --uid 1000 rootless \
    && mkdir -p /home/rootless/.local/share/docker \
    && chown -R rootless /home/rootless \
    && usermod -G sudo rootless

RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

RUN curl -sSL https://github.com/rootless-containers/rootlesskit/releases/download/v1.1.1/rootlesskit-$(uname -m).tar.gz | tar Cxzv /usr/bin
VOLUME /home/rootless/.local/share/docker
ENV DOCKER_HOST=unix:///run/user/1000/docker.sock
COPY ./dockerd-entrypoint.sh /usr/bin
RUN chmod +x  /usr/bin/dockerd-entrypoint.sh

RUN echo 'rootless:root' |chpasswd
RUN mkdir -p /home/rootless/.ssh && \
    chown -R rootless /home/rootless/.ssh
COPY environment /home/rootless/.ssh/
RUN echo "DOCKER_HOST=unix:///run/user/1000/docker.sock" >> /home/rootless/.ssh/environment


